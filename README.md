# README #

This README would normally document whatever steps are necessary to get your application up and running.


### How do I get set up? ###

* Set environment variable (or in `application.yml`) for the books folder. For example: 
`BOOK_PATH:/path/to/books`
* Set up Java 11
* Run `mvn clean package`
* `java -jar target/booksmanager-0.0.1-SNAPSHOT.jar`

### How to use ###

* Create book descriptor:
`curl -d '{"bookName": "Harry Potter", "authorName": "Joanne Rowling", "description": "HP 1"}' -H "Content-Type: application/json" -X POST http://localhost:8080/book-descriptor`
* Get book descriptor:
`curl http://localhost:8080/book-descriptor/Harry%20Potter`
* Update book descriptor:
`curl -d {"bookName": "Harry Potter 2", "authorName": "Joanne Rowling", "description": "HP 2"}' -H "Content-Type: application/json" -X PUT http://localhost:8080/book-descriptor/Harry%20Potter`