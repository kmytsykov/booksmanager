package com.mytsykov.booksmanager.config.properties;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "app")
@Data
public class ApplicationProperties {

    private String booksFolder;

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }
}
