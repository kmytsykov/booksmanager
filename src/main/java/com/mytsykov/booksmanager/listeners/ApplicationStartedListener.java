package com.mytsykov.booksmanager.listeners;

import com.mytsykov.booksmanager.config.properties.ApplicationProperties;
import com.mytsykov.booksmanager.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.IOException;

import static com.mytsykov.booksmanager.common.ErrorMessages.CAN_NOT_INIT_BOOK_STORAGE;

@Component
public class ApplicationStartedListener implements ApplicationListener<ContextRefreshedEvent> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationStartedListener.class);

    private final ApplicationProperties applicationProperties;

    private final FileService fileService;

    @Autowired
    public ApplicationStartedListener(ApplicationProperties applicationProperties,
                                      FileService fileService) {
        this.applicationProperties = applicationProperties;
        this.fileService = fileService;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        initBookFolder();
    }

    private void initBookFolder() {
        try {
            fileService.createFolder(applicationProperties.getBooksFolder());
        } catch (IOException e) {
            LOGGER.error(CAN_NOT_INIT_BOOK_STORAGE);
            throw new RuntimeException(e);
        }
    }
}
