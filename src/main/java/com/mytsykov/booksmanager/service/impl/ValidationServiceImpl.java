package com.mytsykov.booksmanager.service.impl;

import com.google.common.collect.Lists;
import com.mytsykov.booksmanager.dto.BookDescriptorDto;
import com.mytsykov.booksmanager.service.FileService;
import com.mytsykov.booksmanager.service.ValidationService;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.mytsykov.booksmanager.common.ErrorMessages.*;

@Service
public class ValidationServiceImpl implements ValidationService {

    private final FileService fileService;

    @Autowired
    public ValidationServiceImpl(FileService fileService) {
        this.fileService = fileService;
    }

    @Override
    public void validateUniqueness(String bookName) {
        if (fileService.isFileExist(bookName)) {
            throw new IllegalArgumentException(NAME_ALREADY_EXIST);
        }
    }

    @Override
    public void validateForAdd(BookDescriptorDto bookDescriptor) {
        if (bookDescriptor == null || StringUtils.isEmpty(bookDescriptor.getBookName())) {
            throw new IllegalArgumentException(BOOK_DESCRIPTOR_REQUIRED);
        }
        validateUniqueness(bookDescriptor.getBookName());
    }

    @Override
    public void validateForUpdate(String bookName, BookDescriptorDto bookDescriptor) {
        List<String> validationMessages = null;
        if (StringUtils.isEmpty(bookName)) {
            validationMessages = Lists.newArrayList(BOOK_NAME_REQUIRED);
        }
        if (bookDescriptor == null) {
            if (validationMessages == null) {
                validationMessages = Lists.newArrayList();
            }
            validationMessages.add(BOOK_DESCRIPTOR_REQUIRED);
        }

        if (CollectionUtils.isNotEmpty(validationMessages)) {
            throw new IllegalArgumentException(String.join(", ", validationMessages));
        }

        validateUniqueness(bookDescriptor.getBookName());
    }

}
