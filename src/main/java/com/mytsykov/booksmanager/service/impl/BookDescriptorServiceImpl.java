package com.mytsykov.booksmanager.service.impl;

import com.mytsykov.booksmanager.converter.BookDescriptorDtoConverter;
import com.mytsykov.booksmanager.domain.BookDescriptor;
import com.mytsykov.booksmanager.dto.BookDescriptorDto;
import com.mytsykov.booksmanager.service.BookDescriptorService;
import com.mytsykov.booksmanager.service.FileService;
import com.mytsykov.booksmanager.service.ValidationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;

import static com.mytsykov.booksmanager.common.ErrorMessages.BOOK_NOT_FOUND;

@Service
public class BookDescriptorServiceImpl implements BookDescriptorService {

    private static final Logger LOGGER = LoggerFactory.getLogger(BookDescriptorServiceImpl.class);

    private final ValidationService validationService;

    private final BookDescriptorDtoConverter bookDescriptorDtoConverter;

    private final FileService fileService;


    @Autowired
    public BookDescriptorServiceImpl(ValidationService validationService,
                                     BookDescriptorDtoConverter bookDescriptorDtoConverter,
                                     FileService fileService) {
        this.validationService = validationService;
        this.bookDescriptorDtoConverter = bookDescriptorDtoConverter;
        this.fileService = fileService;
    }

    @Override
    public BookDescriptor addBook(BookDescriptorDto bookDescriptorDto) {
        validationService.validateForAdd(bookDescriptorDto);

        BookDescriptor bookDescriptor = bookDescriptorDtoConverter.reverse().convert(bookDescriptorDto);

        fileService.createFile(bookDescriptor.getBookName(), bookDescriptor);

        return bookDescriptor;
    }

    @Override
    public BookDescriptor getBookByName(String bookName) throws FileNotFoundException {
        try {
            return fileService.getFileContent(bookName, BookDescriptor.class);
        } catch (FileNotFoundException e) {
            String message = String.format(BOOK_NOT_FOUND, bookName);
            LOGGER.error(message);
            throw new FileNotFoundException(message);
        }
    }

    @Override
    public BookDescriptor updateBook(String bookName, BookDescriptorDto bookDescriptorDto) throws FileNotFoundException {
        validationService.validateForUpdate(bookName, bookDescriptorDto);

        BookDescriptor bookDescriptor = bookDescriptorDtoConverter.reverse().convert(bookDescriptorDto);

        try {
            fileService.updateFile(bookName, bookDescriptor.getBookName(), bookDescriptor);
            return bookDescriptor;
        } catch (FileNotFoundException e) {
            String message = String.format(BOOK_NOT_FOUND, bookName);
            LOGGER.error(message);
            throw new FileNotFoundException(message);
        }

    }
}
