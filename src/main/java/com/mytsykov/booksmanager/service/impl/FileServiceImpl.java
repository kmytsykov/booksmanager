package com.mytsykov.booksmanager.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mytsykov.booksmanager.config.properties.ApplicationProperties;
import com.mytsykov.booksmanager.service.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

import static com.mytsykov.booksmanager.common.Constants.BOOK_NAME_PATTERN;
import static com.mytsykov.booksmanager.common.Constants.DESCRIPTOR_TYPE;
import static com.mytsykov.booksmanager.common.ErrorMessages.*;

@Service
public class FileServiceImpl implements FileService {

    private static final Logger LOGGER = LoggerFactory.getLogger(FileServiceImpl.class);

    private final ApplicationProperties applicationProperties;

    private final ObjectMapper objectMapper;

    @Autowired
    public FileServiceImpl(ApplicationProperties applicationProperties,
                           ObjectMapper objectMapper) {
        this.applicationProperties = applicationProperties;
        this.objectMapper = objectMapper;
    }

    @Override
    public void createFolder(String folderPath) throws IOException {
        Path path = Paths.get(folderPath);
        if (Files.notExists(path)) {
            Files.createDirectory(path);
        }
    }

    @Override
    public void createFile(String name, Object content) {
        Path path = Paths.get(String.format(BOOK_NAME_PATTERN, applicationProperties.getBooksFolder(), name, DESCRIPTOR_TYPE));
        try {
            if (Files.notExists(path)) {
                Files.createFile(path);
            }
            Files.writeString(path, objectMapper.writeValueAsString(content), StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            LOGGER.error(FILE_CREATION_ERROR);
        }
    }

    @Override
    public void updateFile(String name, String newName, Object content) throws FileNotFoundException {
        if (!isFileExist(name)) {
            throw new FileNotFoundException(String.format(FILE_NOT_FOUND, name));
        }

        createFile(newName, content);
        if (!name.equals(newName)) {
            deleteFile(name);
        }

    }

    @Override
    public Object getFileContent(String name, Class type) throws FileNotFoundException {
        try {
            String fileName = String.format(BOOK_NAME_PATTERN, applicationProperties.getBooksFolder(), name, DESCRIPTOR_TYPE);
            return objectMapper.readValue(Files.readString(Paths.get(fileName)), type);
        } catch (IOException e) {
            LOGGER.error(FILE_READ_EXCEPTION, e);
            throw new FileNotFoundException();
        }
    }

    @Override
    public boolean isFileExist(String name) {
        String fileName = String.format(BOOK_NAME_PATTERN, applicationProperties.getBooksFolder(), name, DESCRIPTOR_TYPE);
        return Files.exists(Paths.get(fileName));
    }

    private void deleteFile(String name) {
        String fileName = String.format(BOOK_NAME_PATTERN, applicationProperties.getBooksFolder(), name, DESCRIPTOR_TYPE);
        try {
            Files.delete(Paths.get(fileName));
        } catch (IOException e) {
            LOGGER.error("Exception occurred while file deleting", e);
            throw new RuntimeException(e);
        }
    }
}
