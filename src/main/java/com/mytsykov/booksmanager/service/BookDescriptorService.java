package com.mytsykov.booksmanager.service;

import com.mytsykov.booksmanager.domain.BookDescriptor;
import com.mytsykov.booksmanager.dto.BookDescriptorDto;

import java.io.FileNotFoundException;

public interface BookDescriptorService {

    BookDescriptor addBook(BookDescriptorDto bookDescriptor);

    BookDescriptor getBookByName(String bookName) throws FileNotFoundException;

    BookDescriptor updateBook(String bookName, BookDescriptorDto bookDescriptor) throws FileNotFoundException;

}
