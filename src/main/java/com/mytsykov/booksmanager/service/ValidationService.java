package com.mytsykov.booksmanager.service;

import com.mytsykov.booksmanager.dto.BookDescriptorDto;

public interface ValidationService {

    void validateUniqueness(String bookName);

    void validateForAdd(BookDescriptorDto bookDescriptor);

    void validateForUpdate(String bookName, BookDescriptorDto bookDescriptor);
}
