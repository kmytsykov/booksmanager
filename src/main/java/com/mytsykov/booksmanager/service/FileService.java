package com.mytsykov.booksmanager.service;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface FileService {

    void createFolder(String path) throws IOException;

    void createFile(String name, Object content);

    void updateFile(String name, String newName, Object content) throws FileNotFoundException;

    <T>T getFileContent(String name, Class<T> type) throws FileNotFoundException;

    boolean isFileExist(String name);
}
