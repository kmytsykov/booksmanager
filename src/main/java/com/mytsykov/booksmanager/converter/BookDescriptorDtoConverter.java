package com.mytsykov.booksmanager.converter;

import com.google.common.base.Converter;
import com.mytsykov.booksmanager.domain.BookDescriptor;
import com.mytsykov.booksmanager.dto.BookDescriptorDto;
import org.springframework.stereotype.Component;

@Component
public class BookDescriptorDtoConverter extends Converter<BookDescriptor, BookDescriptorDto> {

    @Override
    protected BookDescriptorDto doForward(BookDescriptor bookDescriptor) {
        if (bookDescriptor == null)
            return null;

        return BookDescriptorDto.builder()
                .bookName(bookDescriptor.getBookName())
                .authorName(bookDescriptor.getAuthorName())
                .description(bookDescriptor.getDescription())
                .build();
    }

    @Override
    protected BookDescriptor doBackward(BookDescriptorDto bookDescriptorDto) {
        return BookDescriptor.builder()
                .bookName(bookDescriptorDto.getBookName())
                .authorName(bookDescriptorDto.getAuthorName())
                .description(bookDescriptorDto.getDescription())
                .build();
    }
}
