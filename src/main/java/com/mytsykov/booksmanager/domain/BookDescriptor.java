package com.mytsykov.booksmanager.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDescriptor {
    private String bookName;
    private String authorName;
    private String description;
}
