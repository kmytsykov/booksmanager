package com.mytsykov.booksmanager.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDescriptorDto {
    private String bookName;
    private String authorName;
    private String description;
}
