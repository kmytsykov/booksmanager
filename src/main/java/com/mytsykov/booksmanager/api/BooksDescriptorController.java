package com.mytsykov.booksmanager.api;

import com.mytsykov.booksmanager.domain.BookDescriptor;
import com.mytsykov.booksmanager.dto.BookDescriptorDto;
import com.mytsykov.booksmanager.service.BookDescriptorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;

@RestController
@RequestMapping("book-descriptor")
public class BooksDescriptorController {

    private final BookDescriptorService bookDescriptorService;

    @Autowired
    public BooksDescriptorController(BookDescriptorService bookDescriptorService) {
        this.bookDescriptorService = bookDescriptorService;
    }

    @PostMapping
    public BookDescriptor addBook(@RequestBody BookDescriptorDto bookDescriptor) {
        return bookDescriptorService.addBook(bookDescriptor);
    }

    @GetMapping("/{bookName}")
    public BookDescriptor getBook(@PathVariable String bookName) throws FileNotFoundException {
        return bookDescriptorService.getBookByName(bookName);
    }

    @PutMapping("/{bookName}")
    public BookDescriptor updateBook(@PathVariable String bookName,
                                     @RequestBody BookDescriptorDto bookDescriptor) throws FileNotFoundException {
        return bookDescriptorService.updateBook(bookName, bookDescriptor);
    }
}
