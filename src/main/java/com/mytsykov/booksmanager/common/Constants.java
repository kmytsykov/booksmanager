package com.mytsykov.booksmanager.common;

import java.io.File;

public interface Constants {
    String BOOK_NAME_PATTERN = "%s" + File.separator + "%s.%s";

    String DESCRIPTOR_TYPE = "json";
}
