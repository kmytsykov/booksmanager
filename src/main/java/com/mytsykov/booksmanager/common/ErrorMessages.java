package com.mytsykov.booksmanager.common;

public interface ErrorMessages {
    String CAN_NOT_INIT_BOOK_STORAGE = "Can't init books folder";
    String BOOK_NAME_REQUIRED = "Book name required";
    String BOOK_DESCRIPTOR_REQUIRED = "Book descriptor can't be empty";
    String NAME_ALREADY_EXIST = "Such name already exist";
    String BOOK_NOT_FOUND = "Book %s not found";
    String FILE_CREATION_ERROR = "Exception occurred while file creation";
    String FILE_READ_EXCEPTION = "Exception occurred while file reading";
    String FILE_NOT_FOUND = "File %s not found";
}
